// Libraries
import React, {useEffect, useState} from "react"
import { connect } from "react-redux"
import { useLocation } from 'react-router-dom'

// Structure components
import {
    PgContainer,
    PgGridContainerRoot,
    PgGridContainerBody,
    PgTitleH1,
    PgLabel
} from "../../structureComponents"

// Components
import PgNavbar from "../../components/PgNavbar/PgNavbar"
import PgSlider from "../../components/PgSlider/PgSlider"
import PgBadgeItem from "../../components/PgBadgeItem/PgBadgeItem"
import PgDivider from "../../components/PgDivider/PgDivider"
import PgLoading from "../../components/PgLoading/PgLoading"

// Business components
import PropertyBooking from "../../businessComponents/PropertyBooking/PropertyBooking"

// Assets
import locationIcon from "../../assets/icons/location.svg"

// Store
import {fetchPropertyByRouteId} from "../../store/modules/properties/actions"


function Property({ currentProperty, dispatchFetchPropertyByRouteId }) {
    const location = useLocation();

    // States
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        if (location.pathname) {
            handleFetchProperty()
        }
    }, [location.pathname])


    // Methods
    const handleFetchProperty = async () => {
        setLoading(true)
        const routeId = location.pathname.replace("/properties/", "")
        await dispatchFetchPropertyByRouteId({ routeId })

        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }

    if (loading) {
        return (
            <PgContainer fluid height="100vh" flex alignCenter justifyCenter>
                <PgLoading />
            </PgContainer>
        )
    }

    return (
        <PgContainer>
            <PgNavbar />
            <PgContainer pb="40px">
                <PgGridContainerRoot>
                    <PgGridContainerBody>
                        <PgContainer flex justifyCenter padding="0 20px">
                            <PropertyBooking property={currentProperty} />
                        </PgContainer>
                    </PgGridContainerBody>
                </PgGridContainerRoot>
                <PgContainer absolute fluid height="800px" bgColor="#F6DDD1" />
                <PgContainer relative>
                    <PgGridContainerRoot>
                        <PgGridContainerBody>
                            <PgContainer flex alignCenter justifyCenter column mt="60px">
                                <PgTitleH1 xXLarge>{currentProperty?.title}</PgTitleH1>
                                <PgContainer flex wrap alignCenter justifyCenter mt="20px">
                                    <PgContainer>
                                        <PgLabel large>{currentProperty?.maxGuestsLabel}</PgLabel>
                                    </PgContainer>
                                    <PgContainer ml="20px">
                                        <PgLabel large>{currentProperty?.bedroomsLabel}</PgLabel>
                                    </PgContainer>
                                    <PgContainer ml="20px">
                                        <PgLabel large>{currentProperty?.bathroomsLabel}</PgLabel>
                                    </PgContainer>
                                    <PgContainer ml="20px">
                                        <PgLabel large>Private terrasse</PgLabel>
                                    </PgContainer>
                                    <PgContainer ml="20px">
                                        <PgLabel large>Peaceful</PgLabel>
                                    </PgContainer>
                                </PgContainer>
                                <PgContainer mt="20px">
                                    <PgContainer hideOnMobile>
                                        <PgDivider
                                            direction="horizontal"
                                            space="10px"
                                            thickness="2px"
                                            color="#CAB6AD" />
                                    </PgContainer>
                                    <PgContainer flex mcolumn alignCenter padding="0 20px">
                                        <PgContainer>
                                            <PgBadgeItem icon={locationIcon} label={currentProperty?.shortAddress} />
                                        </PgContainer>
                                        <PgContainer hideOnMobile>
                                            <PgDivider
                                                direction="vertical"
                                                height="30px"
                                                thickness="2px"
                                                space="20px"
                                                color="#CAB6AD" />
                                        </PgContainer>
                                        <PgContainer mmt="10px">
                                            <PgBadgeItem
                                            icon={locationIcon}
                                            label="Walk 6 mins (Westbourne Park Station)" />
                                        </PgContainer>
                                        <PgContainer hideOnMobile>
                                            <PgDivider
                                                direction="vertical"
                                                height="30px"
                                                thickness="2px"
                                                space="20px"
                                                color="#CAB6AD" />
                                        </PgContainer>
                                        <PgContainer mmt="10px">
                                            <PgBadgeItem icon={locationIcon} label="Stairs" />
                                        </PgContainer>
                                    </PgContainer>
                                    <PgContainer hideOnMobile>
                                        <PgDivider
                                            direction="horizontal"
                                            space="10px"
                                            thickness="2px"
                                            color="#CAB6AD" />
                                    </PgContainer>
                                </PgContainer>
                            </PgContainer>
                            <PgContainer mt="40px">
                                <PgSlider slides={currentProperty?.images} />
                            </PgContainer>
                        </PgGridContainerBody>
                    </PgGridContainerRoot>
                </PgContainer>
            </PgContainer>
        </PgContainer>
    )
}


const mapStateToProps = (state) => ({
    currentProperty: state.properties.currentProperty
})

const mapDispatchToProps = (dispatch) => ({
    async dispatchFetchPropertyByRouteId({ routeId }) {
        await dispatch(fetchPropertyByRouteId({ routeId }))
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(Property)
