import styled from "styled-components"


export const PropertyBookingRootContainer = styled.div`
    width: 100%;
    @media only screen and (max-width: 1240px) {
      .property-booking--price-per-night {
        display: none;
      }
    }

    @media only screen and (max-width: 1025px) {
        .property-booking--guests {
          display: none;
        }
    }

    @media only screen and (max-width: 769px){
        .property-booking--total-price {
          display: none;
        }
    }
  
    @media only screen and (max-width: 650px) {
      .property-booking--date-range {
        display: none;
      }
    }
  
`
