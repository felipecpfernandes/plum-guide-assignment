// Libraries
import React, {useEffect, useState} from "react"
import { connect } from "react-redux"
import { DateTime } from "luxon"

// Structure components
import { PgContainer } from "../../structureComponents"

// Components
import PgDatePicker from "../../components/PgDatePicker/PgDatePicker"
import PgDivider from "../../components/PgDivider/PgDivider"
import PgSelect from "../../components/PgSelect/PgSelect"
import PgInputAnchor from "../../components/PgInputAnchor/PgInputAnchor"
import PgButton from "../../components/PgButton/PgButton"

// Models
import Booking from "../../models/Booking"

// Business components
import BookingConfirmationDialog from "../BookingConfirmationDialog/BookingConfirmationDialog"

// Style
import { PropertyBookingRootContainer } from "./styled"

// Store
import {setCurrentBooking} from "../../store/modules/bookings/actions"


function PropertyBooking({
    property,
    currentBooking,
    dispatchSetCurrentBooking
}) {
    // State
    const [state, setState] = useState({
        showBookingConfirmationDialog: false,
        _booking: new Booking({
            startDate: null,
            endDate: null,
            guestsAmount: null,
            pricePerNight: null
        })
    })

    // Watchers
    useEffect(() => {
        if (property) {
            const booking = new Booking({
                startDate: DateTime.now().toISODate(),
                endDate: DateTime.now().plus({ days: 2 }).toISODate(),
                guestsAmount: 2,
                pricePerNight: property.pricePerNight
            })

            setState({ ...state, _booking: booking })
            dispatchSetCurrentBooking({ booking })
        }
    }, [property])

    useEffect(() => {
        if (currentBooking) {
            setState({
                ...state,
                _booking: new Booking({
                    ...currentBooking
                })
            })
        }
    }, [currentBooking])

    // Methods
    const handleShowBookingConfirmationDialog = (showBookingConfirmationDialog) => {
        setState({ ...state, showBookingConfirmationDialog })
    }

    const handleGuestsOnChange = (guestsAmount) => {
        const booking = new Booking({
            ...state._booking,
            guestsAmount: guestsAmount.value
        })

       setState({
           ...state,
           _booking: booking
       })

        dispatchSetCurrentBooking({ booking })
    }

    const handleDateRangeOnChange = (dateRange) => {
        const booking = new Booking({
            ...state._booking,
            startDate: dateRange.startDate,
            endDate: dateRange.endDate
        })

        setState({
            ...state,
            _booking: booking
        })

        dispatchSetCurrentBooking({ booking })
    }


    return (
        <PropertyBookingRootContainer>
            <PgContainer
            fluid
            flex
            alignCenter
            justifyCenter
            height="100px"
            mHeight="60px"
            padding="20px 0">
                <PgContainer className="property-booking--date-range">
                    <PgDatePicker
                        autoClose
                        minDate={DateTime.now().toISO()}
                        disableDateInThePast
                        startDate={state._booking.startDate}
                        endDate={state._booking.endDate}
                        onChange={handleDateRangeOnChange}/>
                </PgContainer>
                <PgDivider direction="vertical"  className="property-booking--date-range" />
                <PgContainer className="property-booking--guests">
                    <PgSelect
                        label="For"
                        placeholder="Choose guests"
                        value={state._booking.guestsAmount}
                        items={property.guestsOptions}
                        onChange={handleGuestsOnChange}/>
                </PgContainer>
                <PgDivider direction="vertical" className="property-booking--guests" />
                <PgContainer className="property-booking--price-per-night">
                    <PgInputAnchor label="£ Per night" value={property?.pricePerNight}/>
                </PgContainer>
                <PgDivider direction="vertical" className="property-booking--price-per-night" />
                <PgContainer className="property-booking--total-price">
                    <PgInputAnchor
                        label={`£ Total for ${state._booking.totalNightsLabel}`}
                        value={state._booking.totalPrice}/>
                </PgContainer>

                <PgContainer mFluid ml="40px" mml="0">
                    <PgButton
                    text="Instant Booking"
                    kind="primary"
                    mFluid
                    onClick={() => handleShowBookingConfirmationDialog(true)} />
                </PgContainer>
            </PgContainer>
            <BookingConfirmationDialog
            active={state.showBookingConfirmationDialog}
            onClose={() => handleShowBookingConfirmationDialog(false)} />
        </PropertyBookingRootContainer>
    )
}



const mapStateToProps = (state) => ({
    currentBooking: state.bookings.currentBooking
})

const mapDispatchToProps = (dispatch) => ({
    dispatchSetCurrentBooking({ booking }) {
        dispatch(setCurrentBooking({ booking }))
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(PropertyBooking)
