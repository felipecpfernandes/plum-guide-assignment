// Libraries
import React from "react"

// Components
import PgDrawer from "../../components/PgDrawer/PgDrawer"

// Structure components
import { PgContainer, PgLabel } from "../../structureComponents"


export default function SearchSideMenu({ active, onClose = () => ({}) }) {
    return (
        <PgDrawer direction="right" active={active} onClose={() => onClose()}>
            <PgContainer padding="20px">
                <PgLabel>Coming soon...</PgLabel>
            </PgContainer>
        </PgDrawer>
    )
}
