// Libraries
import React, {useEffect, useState} from "react"
import { DateTime } from "luxon"
import { connect } from "react-redux"

// Components
import PgDialog from "../../components/PgDialog/PgDialog"
import PgInputAnchor from "../../components/PgInputAnchor/PgInputAnchor"
import PgButton from "../../components/PgButton/PgButton"
import PgDivider from "../../components/PgDivider/PgDivider"
import PgDatePicker from "../../components/PgDatePicker/PgDatePicker"
import PgSelect from "../../components/PgSelect/PgSelect"

// Structure components
import { PgContainer, PgLabel } from "../../structureComponents"

// Models
import Booking from "../../models/Booking"

// Store
import { setCurrentBooking } from "../../store/modules/bookings/actions"


function BookingConfirmationDialog({
    currentBooking,
    currentProperty,
    active,
    onClose = () => ({}),
    dispatchSetCurrentBooking
}) {
    // State
    const [state, setState] = useState({
        _booking: new Booking({
            startDate: null,
            endDate: null,
            guestsAmount: null,
            pricePerNight: null
        })
    })

    // Watchers
    useEffect(() => {
        if (currentBooking) {
            setState({
                ...state,
                _booking: new Booking({
                    ...currentBooking
                })
            })
        }
    }, [currentBooking])

    // Methods
    const handleGuestsOnChange = (guestsAmount) => {
        const booking = new Booking({
            ...state._booking,
            guestsAmount: guestsAmount.value
        })

        setState({
            ...state,
            _booking: booking
        })

        dispatchSetCurrentBooking({ booking })
    }

    const handleDateRangeOnChange = (dateRange) => {
        const booking = new Booking({
            ...state._booking,
            startDate: dateRange.startDate,
            endDate: dateRange.endDate
        })

        setState({
            ...state,
            _booking: booking
        })

        dispatchSetCurrentBooking({ booking })
    }


    return (
        <PgDialog active={active} size="small" title="Book now" onClose={() => onClose()}>
            <PgContainer padding="20px" mp="0" fluid>
                <PgContainer fluid maxWidth="28em">
                    <PgLabel medium>
                        You are really close to get your next vacation home.
                        Check the details bellow before proceeding to payment.
                    </PgLabel>
                </PgContainer>
                <PgContainer mt="40px">
                    <PgDivider direction="horizontal" space="20px" />
                    <PgContainer>
                        <PgDatePicker
                            autoClose
                            minDate={DateTime.now().toISO()}
                            startDate={state._booking.startDate}
                            endDate={state._booking.endDate}
                            disableDateInThePast
                            onChange={handleDateRangeOnChange}/>
                    </PgContainer>
                    <PgDivider direction="horizontal" space="20px" />
                    <PgContainer mt="20px">
                        <PgSelect
                            label="For"
                            placeholder="Choose guests"
                            value={state._booking.guestsAmount}
                            items={currentProperty.guestsOptions}
                            onChange={handleGuestsOnChange}/>
                    </PgContainer>
                    <PgDivider direction="horizontal" space="20px" />
                    <PgContainer mt="20px">
                        <PgInputAnchor
                            label={`Total for ${state._booking.totalNightsLabel}`}
                            width="100%"
                            value={`£ ${state._booking.totalPrice}`}/>
                    </PgContainer>
                    <PgDivider direction="horizontal" space="20px" />
                </PgContainer>
                <PgContainer flex alignCenter justifyEnd mt="40px">
                    <PgContainer>
                        <PgButton text="Cancel" onClick={() => onClose()} />
                    </PgContainer>
                    <PgContainer ml="10px">
                        <PgButton kind="primary" text="Payment" />
                    </PgContainer>
                </PgContainer>
            </PgContainer>
        </PgDialog>
    )
}


const mapStateToProps = (state) => ({
    currentBooking: state.bookings.currentBooking,
    currentProperty: state.properties.currentProperty
})

const mapDispatchToProps = (dispatch) => ({
    dispatchSetCurrentBooking({ booking }) {
        dispatch(setCurrentBooking({ booking }))
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(BookingConfirmationDialog)
