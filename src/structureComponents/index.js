const PgContainer = require('./modules/PgContainer').default
const PgTitleH1 = require('./modules/PgTitleH1').default
const PgTitleH2 = require('./modules/PgTitleH2').default
const PgLabel = require('./modules/PgLabel').default
const PgGridContainerRoot = require('./modules/PgGridContainerRoot').default
const PgGridContainerBody = require('./modules/PgGridContainerBody').default


module.exports = {
    PgContainer,
    PgTitleH1,
    PgTitleH2,
    PgLabel,
    PgGridContainerRoot,
    PgGridContainerBody
}
