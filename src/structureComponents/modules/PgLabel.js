import styled from 'styled-components'
import {neutral900} from "../../assets/colorPallet/colorPallet";

const PgLabel = styled.label`
    font-family: 'Roboto', sans-serif;
    color: ${neutral900};
    font-size: ${props => {
        if (props.small) return "12px"    
        else if (props.medium) return "16px"
        else if (props.large) return "18px"
        else if (props.xLarge) return "24px"
        else if (props.xXLarge) return "32px" 
        else if (props.fs) return props.fs        
        else return "14px"
    }};
    font-weight: ${props => props.bold ? "600" : props.semiBold ? "500" : null};
    cursor: ${props => props.pointer ? "pointer" : null};
    white-space: ${props => props.nowrap ? "nowrap" : null};
    text-transform: ${props => props.uppercase ? "uppercase" : null};
    text-align: ${props => props.center ? "center" : null};
    max-width: ${props => props.maxWidth ? props.maxWidth : null};
    line-height: 20px;
    
    @media only screen and (max-width: 600px) {
        font-size: ${props => {
            if (props.small) return "12px"
            else if (props.medium) return "14px"
            else if (props.large) return "16px"
            else if (props.xLarge) return "18px"
            else if (props.xXLarge) return "30px"
            else if (props.mfs) return `${props.mfs} !important`
            else return "14px"
        }};
        white-space: ${props => props.mnowrap ? "nowrap" : null};
    }
`

export default PgLabel
