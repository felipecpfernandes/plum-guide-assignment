import styled from "styled-components"


const PgGridContainerRoot = styled.div`
    display: flex;
    flex-display: column;
    justify-content: center;
    width: 100%;
`

export default PgGridContainerRoot
