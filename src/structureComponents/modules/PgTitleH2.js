import styled from 'styled-components'
import {yellow500, neutral900} from '../../assets/colorPallet/colorPallet'


const PgTitleH2 = styled.h2`
    font-size: ${props => {
        if (props.small) return "16px"
        else if (props.medium) return "18px"
        else if (props.large) return "20px"
        else if (props.xLarge) return "36px"
        else if (props.xXLarge) return "44px"
        else if (props.fs) return props.fs
        else return "18px"
    }};
    font-weight: ${props => props.bold ? "600" : "500"};
    color: ${props => {
        if (props.primary) return yellow500
        else if (props.color) return props.color
        else return neutral900
    }};
    cursor: ${props => props.pointer ? "pointer" : null};
    margin: 0;
    padding: 0;
    
    @media only screen and (max-width: 600px) {
        font-size: ${props => {
            if (props.small) return "16px"
            else if (props.medium) return "18px"
            else if (props.large) return "20px"
            else if (props.xLarge) return "36px"
            else if (props.xXLarge) return "28px"
            else if (props.mfs) return props.mfs
            else return "18px"
        }};
    }
`

export default PgTitleH2
