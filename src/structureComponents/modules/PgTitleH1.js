import styled from 'styled-components'
import {neutral900, yellow500} from "../../assets/colorPallet/colorPallet";


const PgTitleH1 = styled.h1`
    font-size: ${props => {
        if (props.small) return "16px"
        else if (props.fs) return props.fs
        else if (props.medium) return "18px"
        else if (props.large) return "20px"
        else if (props.xLarge) return "22px"
        else if (props.xXLarge) return "56px"
        else return "18px"
    }};
    font-weight: ${props => props.bold ? "600" : "500"};
    color: ${props => {
        if (props.primary) return yellow500
        else if (props.white) return "white"    
        else if (props.color) return props.color
        else return neutral900
    }};
    cursor: ${props => props.pointer ? "pointer" : null};
    margin: 0;
    padding: 0;
    line-height: 64px;
    text-shadow: ${props => props.shadow ? props.shadow : null};
    
    @media only screen and (max-width: 600px) {
      font-size: ${props => {
        if (props.small) return "16px"
            else if (props.medium) return "18px"
            else if (props.large) return "20px"
            else if (props.xLarge) return "22px"
            else if (props.xXLarge) return "36px"
            else if (props.mfs) return props.mfs
            else return "18px"
        }};
      line-height: ${props => props.mlineHeight ? props.mlineHeight : "48px"};
    }
`

export default PgTitleH1
