import styled from "styled-components";

const PgGridContainerBody = styled.div`
    width: 100%;
    max-width:  ${props => props.fluid ? "100%" : "1440px"};
    box-sizing: border-box;
    padding: 0 40px;
  
  @media only screen and (max-width: 600px) {
    max-width: 100%;
    padding: 16px;
  }
`

export default PgGridContainerBody
