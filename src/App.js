import React from "react"
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store/index';

// Pages
import Property from "./pages/Property/Property"

function App() {

  return (
      <Provider store={store}>
          <Router>
              <Switch>
                  <Route
                      exact
                      path="/"
                      component={() => <Redirect to="/properties/monsieur-didot-2-bedrooms-notting-hill-london" />} />
                  <Route
                      exact
                      path="/properties/monsieur-didot-2-bedrooms-notting-hill-london"
                      component={Property} />
              </Switch>
          </Router>
      </Provider>
  );
}

export default App;
