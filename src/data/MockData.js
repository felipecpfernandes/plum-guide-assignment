export const getPropertyByRouteId = async ({routeId}) => {
    const properties = [
        {
            id: 1,
            routeId: "monsieur-didot-2-bedrooms-notting-hill-london",
            title: "Monsieur Didot",
            bedroomsAmount: 2,
            bathroomsAmount: 2,
            maxGuestsAmount: 4,
            city: "London",
            neighborhood: "Notting Hill",
            pricePerNight: 345,
            images: []
        }
    ]

    const property = properties.find(property => property.routeId === routeId)

    return {
        data: property
    }
}
