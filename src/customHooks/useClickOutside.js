import React, {useEffect, useState} from "react"

/**
 * Hook that alerts clicks outside of the passed ref
 */
export default function useClickOutside(ref) {
    const [clickedOutside, setClickedOutside] = useState(false)

    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setClickedOutside(true)
            }

            setClickedOutside(false)
        }

        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);

    return { clickedOutside }
}
