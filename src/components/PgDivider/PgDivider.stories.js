import PgDivider from './PgDivider'
import { PgLabel, PgContainer } from "../../structureComponents";

export default {
    title: 'Display/PgDivider',
    component: PgDivider
}

const Template = (args) => {
    return (
        <div>
            <PgLabel>This is something...</PgLabel>
                <PgDivider {...args} />
            <PgLabel>This is something else...</PgLabel>
        </div>
    )
}

export const horizontal = Template.bind({})
horizontal.args = {
    direction: "horizontal",
    height: "20px"
}

const TemplateVertical = (args) => {
    return (
        <PgContainer flex alignCenter>
            <PgLabel>This is something...</PgLabel>
            <PgDivider {...args} />
            <PgLabel>This is something else...</PgLabel>
        </PgContainer>
    )
}

export const vertical = TemplateVertical.bind({})

vertical.args = {
    direction: "vertical",
    height: "20px"
}
