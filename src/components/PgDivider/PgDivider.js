// Libraries
import React from "react"

// Styles
import {PgDividerRootContainer} from "./styled";


/**
 *
 * @param {string} direction
 * @param {string} height
 * @param {string} space
 * @param {string} color
 * @param {string} thickness
 * @param {string} className
 */
export default function PgDivider({
  direction = "vertical",
  height,
  space,
  color,
  thickness,
  className
}) {
    return (
        <PgDividerRootContainer
        className={className}
        direction={direction}
        height={height}
        space={space}
        thickness={thickness}
        color={color} />
    )
}
