// Libraries
import styled from "styled-components"
import {neutral100} from "../../assets/colorPallet/colorPallet";


export const PgDividerRootContainer = styled.div`
    width: ${props => props.direction === "horizontal" ? "100%" : null};
    border-right: ${props => props.direction === "vertical" ? `${props.thickness || "1px"} solid ${props.color ? props.color : neutral100}` : null};
    border-left: none;
    border-top: none;
    margin: ${props => props.direction === "vertical" ? `0 ${props.space || "40px"}` : `${props.space || "40px"} 0`};
    border-bottom: ${props => props.direction === "horizontal" ? `${props.thickness || "1px"} solid ${props.color ? props.color : neutral100}` : null};
    height: ${props => props.direction === "vertical" ? props.height || "100%" : "1px"};
`
