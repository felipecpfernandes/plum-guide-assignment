// Libraries
import React, { useEffect, useRef, useState } from "react"

// Styles
import { PgSelectRootContainer, PgSelectDropdownContainer } from "./styled"

// Components
import PgInputAnchor from "../PgInputAnchor/PgInputAnchor"
import PgDropdown from "../PgDropdown/PgDropdown"

// Custom hooks
import useClickOutside from "../../customHooks/useClickOutside"


/**
 *
 * @param {array} items
 * @param {string} label
 * @param {string} placeholder
 * @param {string} itemValue
 * @param {string} itemLabel
 * @param {string | boolean | number} value
 * @param {function} onChange
 */
export default function PgSelect({
    items = [],
    label,
    placeholder,
    itemValue = "value",
    itemLabel = "label",
    value,
    onChange = () => ({})
}) {
    // Refs
    const pgSelectRef = useRef(null)

    // Hooks
    const { clickedOutside } = useClickOutside(pgSelectRef)

    // State
    const [state, setState] = useState({
        selectedItem: null,
        showDropdown: false
    })

    // Watchers
    useEffect(() => {
        if (clickedOutside) {
            setState({ ...state, showDropdown: false })
        }
    }, [clickedOutside])

    useEffect(() => {
        if (value && items.length > 0) {
            const selectedItem = items.find(item => item[itemValue] === value)
            setState({ ...state, selectedItem })
        }
    }, [value])

    // Methods
    const handleShowDropdown = (showDropdown) => {
        setState({ ...state, showDropdown })
    }

    const handleOnSelect = (selectedItem) => {
        setState({
            ...state,
            selectedItem,
            showDropdown: false
        })

        onChange(selectedItem)
    }


    return (
        <PgSelectRootContainer ref={pgSelectRef}>
            <PgInputAnchor
            label={label}
            active={state.showDropdown}
            placeholder={placeholder}
            value={state.selectedItem?.label}
            kind="select"
            onChange={handleShowDropdown}
            />
            <PgSelectDropdownContainer active={state.showDropdown}>
                <PgDropdown
                items={items}
                itemsLabel={itemLabel}
                itemValue={itemValue}
                onChange={handleOnSelect}/>
            </PgSelectDropdownContainer>
        </PgSelectRootContainer>
    )
}
