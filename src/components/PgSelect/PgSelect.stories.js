import PgSelect from './PgSelect'

export default {
    title: 'Form/PgSelect',
    component: PgSelect,
}

const Template = (args) => {
    return (
        <div>
            <PgSelect {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    label: "For",
    placeholder: "Choose guests",
    items: [
        { label: "1 guest", value: 1 },
        { label: "2 guests", value: 2 },
        { label: "3 guests", value: 3 },
        { label: "4 guests", value: 4 },
        { label: "5 guests", value: 5 },
        { label: "6 guests", value: 6 },
        { label: "7 guests", value: 7 },
        { label: "8 guests", value: 8 },
        { label: "9 guests", value: 9 },
        { label: "10 guests", value: 10 },
        { label: "11 guests", value: 11 },
    ]
}
