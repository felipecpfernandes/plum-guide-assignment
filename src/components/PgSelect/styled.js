// Libraries
import styled from "styled-components"


export const PgSelectRootContainer = styled.div`
    
`

export const PgSelectDropdownContainer = styled.div`
    margin-top: 20px;
    margin-top: ${props => props.active ? "20px" : "0"};
    opacity: ${props => props.active ? 1 : 0};
    pointer-events: ${props => props.active ? "all" : "none"};
    transition: opacity .2s ease-in-out, margin-top .2s ease-in-out;
    position: absolute;
    z-index: 100;
    pointer-events: ${props => props.active ? "all" : "none"};
    visibility: ${props => !props.active ? "hidden" : "visible"};
`

