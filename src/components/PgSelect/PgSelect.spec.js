import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgSelect from './PgSelect';

describe("PgSelect", () => {
    const items = [
        { label: "1 Guest", value: 1 },
        { label: "2 Guests", value: 2 },
        { label: "3 Guests", value: 3 }
    ]

    it('should render the items correctly', () => {
        mount(<PgSelect items={items} />)

        cy.getCy("pg-input-anchor").click()

        cy.get(`.pg-dropdown`).contains("1 Guest")
        cy.get(`.pg-dropdown`).contains("2 Guests")
        cy.get(`.pg-dropdown`).contains("3 Guests")

        cy.get(`.pg-dropdown--item__${items[1].value}`).click()


        cy.getCy("pg-input-anchor").contains("2 Guests")
    });
})
