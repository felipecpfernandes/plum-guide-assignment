// Libraries
import React from "react"
import { ReactSVG } from "react-svg"

// Styles
import { SvgIconContainer } from "./styled"


/**
 *
 * @param {string} src
 * @param {string} color
 * @param {string} size
 */
export default function PgSvg({
     src,
     color,
     size = "24px"
}) {
    return (
        <SvgIconContainer color={color} size={size}>
            <ReactSVG src={src} className="svg-icon--container" />
        </SvgIconContainer>
    )
}
