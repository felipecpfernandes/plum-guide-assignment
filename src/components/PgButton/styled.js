import styled from "styled-components"
import {neutral100, neutral900, yellow500} from "../../assets/colorPallet/colorPallet"


export const PgButtonRootContainer = styled.button`
    padding: ${props => {
       if (props.kind === "icon" || props.kind === "icon-outlined" || props.slimSize) {
           return "10px"
       } 
       
       if (props.kind === "accessibility-only") {
           return "0"
       }
       
       return "10px 30px"
    }};
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${props => {
       if (props.kind === "icon" || props.kind === "icon-outlined" || props.slimSize) {
            return "40px"
       }
       
       if (props.kind === "accessibility-only") {
            return "auto"
       }
       
       return "50px"
    }};
    background-color: ${props => {
        switch(props.kind) {
          case "primary":
              return yellow500
          case "icon":
              return "transparent"
          case "icon-outlined":
              return "#ffffff61"
          default:
              return "transparent"
                
        }
    }};
    border-radius: ${props => {
        if (props.kind === "icon" || props.kind === "icon-outlined") {
            return "50%"
        }
        
        return null
    }};
    border: ${props => {
        if (props.kind === "icon-outlined") {
            return "1px solid #fff"
        }
        
        return "none"
    }};
    cursor: pointer;
    color: ${neutral900};
    font-size: ${props => {
        if (props.slimSize) {
          return "14px"
        }
        
        if (props.kind === "accessibility-only") {
            return "unset"
        }
        
        return "16px"
    }};
    font-weight: 500;
    width: ${props => {
       if (props.kind === "icon" || props.kind === "icon-outlined") {
           return "40px"
       }
       
       if (props.fluid) {
           return "100%"
       }
    }};
    transition: background-color .2s ease-in-out;
    text-transform: ${props => props.uppercase ? "uppercase" : null};
  
    &:hover {
      background-color: ${props => {
          switch(props.kind) {
            case "primary":
                return yellow500
            case "icon" || "icon-outlined":
                return neutral100
            case "accessibility-only":
                return "transparent"
            default:
                return neutral100
          }
      }};

      svg {
        width: ${props => props.kind === "icon-outlined" ? neutral900 : null};
        height: auto;
        fill: ${props => props.kind === "icon-outlined" ? neutral900 : null};

        g {
          fill: ${props => props.kind === "icon-outlined" ? neutral900 : null};
          path {
            fill: ${props => props.kind === "icon-outlined" ? neutral900 : null};
          }
        }
      }
    }
  
  @media only screen and (max-width: 600px) {
    width: ${props => props.mFluid ? "100%" : null};
  }
`
