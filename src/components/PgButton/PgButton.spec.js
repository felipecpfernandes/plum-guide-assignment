import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgButton from './PgButton';

describe("PgButton", () => {
    const yellow500RGB = "rgb(253, 187, 48)"
    const transparentRGB = "rgba(0, 0, 0, 0)"

    it('should work correctly when the kind is primary', () => {
        mount(<PgButton kind="primary" text="Click me" />)
        cy.get(".pg-button").contains("Click me")
        cy.get(".pg-button").should(($el) => {
            expect($el).to.have.css("background-color", yellow500RGB)
        })
    });

    it('should work correctly when no kind is set', () => {
        mount(<PgButton text="Click me" />)
        cy.get(".pg-button").contains("Click me")
        cy.get(".pg-button").should(($el) => {
            expect($el).to.have.css("background-color", transparentRGB)
        })
    });

    it('should work correctly when the fluid prop is set', () => {
        cy.viewport(500, 750)
        mount(<PgButton text="Click me" fluid />)
        cy.get(".pg-button").contains("Click me")
        cy.get(".pg-button").should(($el) => {
            expect($el).to.have.css("width", `${500 - 16}px`)
        })
    });

    it('should work correctly when the uppercase prop is set', () => {
        mount(<PgButton text="Click me" uppercase />)
        cy.get(".pg-button").contains("Click me")
        cy.get(".pg-button").should(($el) => {
            expect($el).to.have.css("text-transform", "uppercase")
        })
    });
})
