import PgButton from './PgButton'
import locationIcon from "../../assets/icons/location.svg"

export default {
    title: 'Form/PgButton',
    component: PgButton,
    argTypes: {
        kind: {
            control: {
                type: 'select',
                options: ['default', 'primary', 'accessibility-only', 'icon']
            }
        },
        icon: {
            control: {
                type: 'select',
                options: [locationIcon]
            }
        }
    }
}

const Template = (args) => {
    return (
        <div>
            <PgButton {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    text: "Instant bookings",
    kind: "primary",
    uppercase: true
}
