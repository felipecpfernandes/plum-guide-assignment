// Libraries
import React from "react"

// Styles
import {PgButtonRootContainer} from "./styled"

// Components
import PgSvg from "../PgSvg/SvgIcon"

// Assets
import {neutral900} from "../../assets/colorPallet/colorPallet"


/**
 *
 * @param {string} kind
 * @param {boolean} slimSize
 * @param {string} text
 * @param {string} icon
 * @param {boolean} fluid
 * @param {boolean} mFluid
 * @param {string} iconSize
 * @param {string} color
 * @param {boolean} uppercase
 * @param {string} className
 * @param {function} onClick
 */
export default function PgButton({
    kind,
    slimSize,
    text,
    icon,
    fluid,
    mFluid,
    iconSize = "12px",
    color = neutral900,
    uppercase,
    className,
    onClick = () => ({})
 }) {
    return (
        <PgButtonRootContainer
            className={`pg-button ${className ? className : ""}`}
            fluid={fluid}
            mFluid={mFluid}
            slimSize={slimSize}
            kind={kind}
            uppercase={uppercase}
            onClick={() => onClick()}>
            {icon ? (
                <PgSvg src={icon} size={iconSize} color={color} />
            ) : (
                text
            )}
        </PgButtonRootContainer>
    )
}
