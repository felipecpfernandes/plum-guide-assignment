// Libraries
import styled from "styled-components"
import {neutral100} from "../../assets/colorPallet/colorPallet";


export const PgDropdownRootContainer = styled.div`
    background-color: white;
    box-shadow: 1px 1px 8px #00000017;
    padding: 20px;
    box-sizing: border-box;
    position: absolute;
    overflow: auto;
    max-height: 50vh;
    width: ${props => props.width ? props.width : "12em"};

    //CUSTOM SCROLL BAR

    &::-webkit-scrollbar {
        width: 6px;
        background-color: transparent;
    }
    
    &::-webkit-scrollbar-track {
        width: 1px;
        background-color: transparent;
    }
    
    &::-webkit-scrollbar-thumb {
        width: 1px;
        background-color: #ccc;
        -webkit-box-shadow: inset 1px 1px 0px #eee;
    }
`

export const PgDropdownList = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;
`

export const PgDropdownListItem = styled.li`
    padding: 10px 20px;
    cursor: pointer;
    transition: background-color .2s ease;
    background-color: ${props => props.active ? neutral100 : null};
  
    &:hover {
      background-color: ${neutral100};
    }
`
