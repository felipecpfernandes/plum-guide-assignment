import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgDropdown from './PgDropdown';

describe("PgDropdown", () => {
    const items = [
        { label: "1 Guest", value: 1 },
        { label: "2 Guests", value: 2 },
        { label: "3 Guests", value: 3 }
    ]

    it('should render the items correctly', () => {
        mount(<PgDropdown items={items} />)

        cy.get(`.pg-dropdown`).contains("1 Guest")
        cy.get(`.pg-dropdown`).contains("2 Guests")
        cy.get(`.pg-dropdown`).contains("3 Guests")
    });

    it('should select an item correctly', () => {
        mount(<PgDropdown items={items} itemLabel="label" itemValue="value" onChange={() => ({})} />)

        cy.get(`.pg-dropdown--item__${items[1].value}`).click()

        cy.get(`.pg-dropdown--item__${items[1].value}`).should(($el) => {
            const classList = Array.from($el[0].classList);
            expect(classList).includes("pg-dropdown--item__active")
        })
    });
})
