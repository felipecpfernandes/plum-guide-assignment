// Libraries
import React, {useState} from "react"

// Styles
import {PgDropdownRootContainer, PgDropdownList, PgDropdownListItem} from "./styled";

// Components
import PgButton from "../PgButton/PgButton";


/**
 *
 * @param {array} items
 * @param {string} itemLabel
 * @param {string} itemValue
 * @param {string} width
 * @param {function} onChange
 */
export default function PgDropdown({
    items = [],
    itemLabel = "label",
    itemValue = "value",
    width,
    onChange = () => ({})
}){
    // State
    const [state, setState] = useState({
        internalValue: null
    })

    // Methods
    const handleOnSelect = (selectedItem) => {
        setState({ ...state, internalValue: selectedItem[itemValue] })
        onChange(selectedItem)
    }

    return (
        <PgDropdownRootContainer width={width} className="pg-dropdown">
            <PgDropdownList>
                {items.map((item, index) => (
                    <PgDropdownListItem
                        key={`${item.value}__${index}`}
                        className={`
                            pg-dropdown--item__${item.value} 
                            ${item[itemValue] === state.internalValue ? "pg-dropdown--item__active" : ""}
                        `}
                        active={item[itemValue] === state.internalValue}
                        onClick={() => handleOnSelect(item)}>
                        <PgButton kind="accessibility-only" text={item[itemLabel]}  />
                    </PgDropdownListItem>
                ))}
            </PgDropdownList>
        </PgDropdownRootContainer>
    )
}
