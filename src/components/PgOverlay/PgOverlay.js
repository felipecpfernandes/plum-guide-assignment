// Libraries
import React from "react"

// Styles
import {PgOverlayRootContainer} from "./styled";


export default function PgOverlay({ active }) {
    return <PgOverlayRootContainer active={active} />
}
