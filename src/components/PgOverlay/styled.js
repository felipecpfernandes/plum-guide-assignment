// Libraries
import styled from "styled-components"

export const PgOverlayRootContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background-color: #0000001f;
  z-index: 1000;
  opacity: ${props => props.active ? 1 : 0};
  pointer-events: ${props => props.active ? "all" : "none"};
  transition: opacity .2s ease;
`
