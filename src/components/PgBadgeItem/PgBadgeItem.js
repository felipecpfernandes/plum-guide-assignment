// Libraries
import React from "react"

// Components
import PgSvg from "../PgSvg/SvgIcon"

// Color pallet
import {neutral900} from "../../assets/colorPallet/colorPallet"

// Styles
import {
    PgBadgeItemRootContainer,
    PgBadgeItemIconContainer,
    PgBadgeItemLabel
} from "./styled"


/**
 * @param {string} label
 * @param {string} icon
 */
export default function PgBadgeItem({
    label,
    icon
}) {
    return (
        <PgBadgeItemRootContainer className="pg-badge-item">
            {icon && (
                <PgBadgeItemIconContainer>
                    <PgSvg src={icon} size="14px" color={neutral900} />
                </PgBadgeItemIconContainer>
            )}
            <PgBadgeItemLabel>{label}</PgBadgeItemLabel>
        </PgBadgeItemRootContainer>
    )
}
