import PgBadgeItem from './PgBadgeItem'
import locationIcon from "../../assets/icons/location.svg"

export default {
    title: 'Display/PgBadgeItem',
    component: PgBadgeItem
}

const Template = (args) => {
    return (
        <div>
            <PgBadgeItem {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    label: "Notting Hill, London",
    icon: locationIcon
}
