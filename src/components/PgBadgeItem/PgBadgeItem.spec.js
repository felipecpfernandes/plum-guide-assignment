import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgBadgeItem from './PgBadgeItem';


describe("PgBadgeItem", () => {
    it('should work correctly when the label prop is set', () => {
        mount(<PgBadgeItem label="Notting Hill"  />)

        cy.get(".pg-badge-item").contains("Notting Hill")
    });

})
