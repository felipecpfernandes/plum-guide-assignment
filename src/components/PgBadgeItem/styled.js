// Libraries
import styled from "styled-components"


export const PgBadgeItemRootContainer = styled.div`
  display: flex;
  align-items: center;
`

export const PgBadgeItemIconContainer = styled.div`
  margin-right: 10px;
`

export const PgBadgeItemLabel = styled.label`
    font-size: 16px;
  
  @media only screen and (max-width: 600px) {
    font-size: 12px;
  }
`
