// Libraries
import React from "react"

// Styles
import { PgSlideArrowRootContainer } from "./styled"

// Components
import PgButton from "../../PgButton/PgButton"

// Assets
import arrowNextIcon from "../../../assets/icons/arrow-next.svg"
import arrowBackIcon from "../../../assets/icons/arrow-back.svg"


/**
 *
 * @param {string} direction
 * @param {string} className
 * @param {function} onClick
 */
export default function PgSlideArrow({
    direction,
    className,
    onClick = () => ({})
}) {
    return (
        <PgSlideArrowRootContainer direction={direction} className={className}>
            {direction === "right" ? (
                <PgButton kind="icon-outlined" iconColor="white" icon={arrowNextIcon} onClick={() => onClick()} />
            ) : (
                <PgButton kind="icon-outlined" iconColor="white" icon={arrowBackIcon} onClick={() => onClick()} />
            )}
        </PgSlideArrowRootContainer>
    )
}
