// Libraries
import styled from "styled-components"


export const PgSlideArrowRootContainer = styled.div`
  display: flex;
  position: absolute;
  top: 50%;
  right: ${props => props.direction === "right" ? "25px" : null};
  left: ${props => props.direction === "left" ? "25px" : null};
  height: 50px;
  width: 50px;
  justify-content: center;
  transition: transform ease-in 0.2s;
  
  button {
    transform: translateX(${props => props.direction === 'left' ? '-2' : '2'}px);
  }
  
  &:hover {
    transform: scale(1.1);
  }
`
