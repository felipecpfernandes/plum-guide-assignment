// Libraries
import React from "react"

// Styles
import { PgSlideRootContainer } from "./styled"

export default function PgSlide({ imageUrl, className }) {
    return (
        <PgSlideRootContainer imageUrl={imageUrl} className={className} />
    )
}
