// Libraries
import styled from "styled-components"


export const PgSlideRootContainer = styled.div`
    height: 100%;
    width: 100%;
    background-image: url('${props => props.imageUrl}');
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
`
