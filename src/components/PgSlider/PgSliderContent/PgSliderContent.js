// Libraries
import React from "react"

// Styles
import { PgSliderContentContainer } from "./styled"


/**
 *
 * @param {string} translate
 * @param {string} transition
 * @param {string} width
 * @param {node} children
 */
export default function PgSliderContent({
    translate,
    transition,
    width,
    children
}) {
    return (
        <PgSliderContentContainer
        className="SliderContent"
        translate={translate}
        transition={transition} width={width}>
            {children}
        </PgSliderContentContainer>
    )
}
