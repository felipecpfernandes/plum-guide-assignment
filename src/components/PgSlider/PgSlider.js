// Libraries
import React, {useEffect, useRef, useState} from "react"

// Styles
import {
    PgSliderRootContainer,
    PgSliderPaginationContainer,
    PgSliderPaginationLabel
} from "./styled"

// Partials
import PgSlide from "./PgSlide/PgSlide"
import PgSlideArrow from "./PgSlideArrow/PgSlideArrow"
import PgSliderContent from "./PgSliderContent/PgSliderContent"


const getWidth = () => window.innerWidth


export default function PgSlider({ slides = [] }) {
    // State
    const [state, setState] = useState({
        activeSlide: 0,
        translate: getWidth(),
        transition: 0.45,
        firstSlide: null,
        secondSlide: null,
        lastSlide: null,
        transitioning: false,
        _slides: []
    })

    // Refs
    const sliderRef = useRef()
    const transitionRef = useRef()
    const resizeRef = useRef()

    useEffect(() => {
        transitionRef.current = smoothTransition
        resizeRef.current = handleResize
    })

    // Mounted
    useEffect(() => {
        const slider = sliderRef.current

        const smooth = e => {
            if (e.target.className.includes('SliderContent')) {
                transitionRef.current()
            }
        }

        const resize = () => {
            resizeRef.current()
        }

        const transitionEnd = slider.addEventListener('transitionend', smooth)
        const onResize = window.addEventListener('resize', resize)

        return () => {
            slider.removeEventListener('transitionend', transitionEnd)
            window.removeEventListener('resize', onResize)
        }
    }, [])

    // Watchers
    useEffect(() => {
        if (state.transition === 0) setState({ ...state, transition: 0.45, transitioning: false  })
    }, [state.transition])

    useEffect(() => {
        const resize = () => {
            resizeRef.current()
        }

        const onResize = window.addEventListener('resize', resize)

        return () => {
            window.removeEventListener('resize', onResize)
        }
    }, [sliderRef])

    useEffect(() => {
        if (slides.length > 0) {
            const firstSlide = slides[0]
            const secondSlide = slides[1]
            const lastSlide = slides[slides.length - 1]

            setState({
                ...state,
                firstSlide,
                secondSlide,
                lastSlide,
                _slides: [lastSlide, firstSlide, secondSlide]
            })
        }
    }, [slides])


    // Methods
    const nextSlide = () => {
        if(state.transitioning) return

        setState({
            ...state,
            translate: state.translate + getWidth(),
            activeSlide: state.activeSlide === slides.length - 1 ? 0 : state.activeSlide + 1,
            transitioning: true
        })
    }

    const prevSlide = () => {
        if(state.transitioning) return

        setState({
            ...state,
            translate: 0,
            activeSlide: state.activeSlide === 0 ? slides.length - 1 : state.activeSlide - 1,
            transitioning: true
        })
    }

    const handleResize = () => {
        setState({ ...state, translate: getWidth(), transition: 0 })
    }

    const smoothTransition = () => {
        let _slides = []

        if (state.activeSlide === slides.length - 1) {
            _slides = [slides[slides.length - 2], state.lastSlide, state.firstSlide]
        } else if (state.activeSlide === 0) {
            _slides = [state.lastSlide, state.firstSlide, state.secondSlide]
        } else {
            _slides = slides.slice(state.activeSlide - 1, state.activeSlide + 2)
        }

        setState({
            ...state,
            _slides,
            transition: 0,
            translate: getWidth()
        })
    }


    return (
        <PgSliderRootContainer ref={sliderRef} className="pg-slider">
            <PgSliderContent
            translate={state.translate}
            transition={state.transition}
            width={getWidth() * state._slides.length}>
                {state._slides.map((_slide, i) => (
                    <PgSlide
                        width={getWidth()}
                        className={`pg-slider--slide__${i}`}
                        key={_slide + i}
                        imageUrl={_slide} />
                ))}
            </PgSliderContent>
            <PgSlideArrow direction="left" onClick={prevSlide} className="pg-slider--back" />
            <PgSlideArrow direction="right" onClick={nextSlide} className="pg-slider--next" />
            <PgSliderPaginationContainer>
                <PgSliderPaginationLabel className="pg-slider--pagination">
                    {state.activeSlide + 1}/{slides.length}
                </PgSliderPaginationLabel>
            </PgSliderPaginationContainer>
        </PgSliderRootContainer>
    )
}
