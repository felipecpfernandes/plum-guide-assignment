// Libraries
import styled from "styled-components"


export const PgSliderRootContainer = styled.div`
    position: relative;
    height: 600px;
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
`

export const PgSliderPaginationContainer = styled.div`
  z-index: 10000000;
  position: absolute;
  left: 50%;
  background-color: #ffffffad;
  width: 40px;
  height: 30px;
  border-radius: 20px;
  padding: 0 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  bottom: 20px;
  transform: translateX(-50%);
`

export const PgSliderPaginationLabel = styled.label`
  font-size: 12px;
`
