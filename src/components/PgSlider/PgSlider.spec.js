import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgSlider from './PgSlider';

describe("PgSlider", () => {
    it('should navigate correctly', () => {
        const slides =  [
            'https://images.unsplash.com/photo-1449034446853-66c86144b0ad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80',
            'https://images.unsplash.com/photo-1470341223622-1019832be824?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2288&q=80',
            'https://images.unsplash.com/photo-1448630360428-65456885c650?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2094&q=80',
            'https://images.unsplash.com/photo-1534161308652-fdfcf10f62c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2174&q=80'
        ]


        mount(<PgSlider slides={slides} />)

        // Navigate to next slides
        cy.get(".pg-slider--pagination").contains("1/4")
        cy.get(".pg-slider--slide__1").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[0]}")`)
        })

        cy.get(".pg-slider--next").click()
        cy.get(".pg-slider--pagination").contains("2/4")
        cy.get(".pg-slider--slide__2").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[1]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--next").click()
        cy.get(".pg-slider--pagination").contains("3/4")
        cy.get(".pg-slider--slide__1").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[2]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--next").click()
        cy.get(".pg-slider--pagination").contains("4/4")
        cy.get(".pg-slider--slide__2").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[3]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--next").click()
        cy.get(".pg-slider--pagination").contains("1/4")
        cy.get(".pg-slider--slide__1").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[0]}")`)
        })

        // Navigate to previous slides
        cy.wait(1000)
        cy.get(".pg-slider--back").click()
        cy.get(".pg-slider--pagination").contains("4/4")
        cy.get(".pg-slider--slide__2").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[1]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--back").click()
        cy.get(".pg-slider--pagination").contains("3/4")
        cy.get(".pg-slider--slide__1").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[2]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--back").click()
        cy.get(".pg-slider--pagination").contains("2/4")
        cy.get(".pg-slider--slide__2").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[3]}")`)
        })

        cy.wait(1000)

        cy.get(".pg-slider--back").click()
        cy.get(".pg-slider--pagination").contains("1/4")
        cy.get(".pg-slider--slide__1").should(($el) => {
            expect($el).to.have.css("background-image", `url("${slides[0]}")`)
        })
    });
})
