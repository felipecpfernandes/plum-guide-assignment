import styled from "styled-components"


export const PgCalendarRootContainer = styled.div`
    width: 100%;
    max-width: 20em;
    padding: 20px;
    box-sizing: border-box;
    background-color: white;
    border: ${props => props.kind === "outlined" ? "1px solid #eee" : null};
`

export const PgCalendarHeaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`
export const PgCalendarArrowBackContainer = styled.div`
    
`

export const PgCalendarMonthNameCalendar = styled.div`
    font-weight: 600;
`

export const PgCalendarNextArrowContainer = styled.div`
    
`

export const PgCalendarGridContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
    margin-top: 10px;
`

export const PgCalendarGridSquareContainer = styled.div`
    border: ${props => props.active ? "1px solid #fdbb30" : "1px solid #eee"};
    display: flex;
    align-items: center;
    padding: 10px;
    justify-content: center;
    cursor: pointer;
    height: 15px;
    pointer-events: ${props => props.disabled ? "none" : "all"};
    opacity: ${props => props.disabled ? .4 : 1};
    background-color: ${props => props.active ? "#fdbb30" : props.highlight ? "#fff7eb" : null};
  
    &:hover {
      background-color: ${props => props.active ? "#fdbb30" : "#fff7eb"};
    }
`

export const PgCalendarWeekDaysNameContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
`

export const PgCalendarWeekDayContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`

export const PgCalendarWeekDayLabel = styled.label`

`

export const PgCalendarDayContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`
