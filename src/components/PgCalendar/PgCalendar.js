// libraries
import React, {useEffect, useState} from "react"
import { DateTime } from "luxon"

// Components
import PgButton from "../PgButton/PgButton"

// Styles
import {
    PgCalendarRootContainer,
    PgCalendarHeaderContainer,
    PgCalendarArrowBackContainer,
    PgCalendarMonthNameCalendar,
    PgCalendarNextArrowContainer,
    PgCalendarGridContainer,
    PgCalendarGridSquareContainer,
    PgCalendarWeekDaysNameContainer,
    PgCalendarWeekDayLabel,
    PgCalendarWeekDayContainer,
    PgCalendarDayContainer,
} from "./styled"

// Icons
import arrowBackIcon from "../../assets/icons/arrow-back.svg"
import arrowNextIcon from "../../assets/icons/arrow-next.svg"


/**
 * @param {string} kind
 * @param {string} startDate
 * @param {string} endDate
 * @param {string} minDate
 * @param {boolean} disableDateInThePast
 * @param {function} onChange
 */
export default function PgCalendar({
    kind,
    startDate,
    endDate,
    minDate,
    disableDateInThePast,
    onChange = () => ({})
}) {
    // State
    const [state, setState] = useState({
        gridDays: [],
        weekDays: [
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat",
            "Sun"
        ],
        selectedMonthDateTime: DateTime.now(),
        currentMonthDays: null,
        firstWeekDayOfMonth: null,
        selectedStartDate: null,
        selectedEndDate: null,
    })

    // Watchers
    useEffect(() => {
        if (state.selectedMonthDateTime) {
            generateDays()
        }
    }, [state.selectedMonthDateTime])

    useEffect(() => {
        if (state.selectedStartDate && state.selectedEndDate) {
            setState({ ...state, selectedMonthDateTime: DateTime.fromISO(state.selectedStartDate) })
            onChange({
                startDate: state.selectedStartDate,
                endDate: state.selectedEndDate,
            })
        }
    }, [state.selectedStartDate, state.selectedEndDate])

    useEffect(() => {
        if (
            (startDate && startDate !== state.selectedStartDate)
            && (endDate && endDate !== state.selectedEndDate)
        ) {
            setState({
                ...state,
                selectedStartDate: startDate,
                selectedEndDate: endDate,
                selectedMonthDateTime: DateTime.fromISO(startDate)
            })
        }
    }, [startDate, endDate])

    // Methods
    const generateDays = () => {
        const gridDays = []

        let day = 1
        let firstDayOfMonth = state.selectedMonthDateTime.set({ day: 1 })
        let limit = 35

        const monthOffset = (state.selectedMonthDateTime.daysInMonth - firstDayOfMonth.weekday) / 4
        const weekOffset = firstDayOfMonth.weekday - 1

        if (monthOffset <= 6) limit += 7

        for(let i = 0; i < limit; i += 1) {
            const dayDateTime = state.selectedMonthDateTime.set({
                day: (i + 1)  - weekOffset
            })

            if (i + 1 >= firstDayOfMonth.weekday && day <= state.selectedMonthDateTime.daysInMonth) {
                const dateISOString = dayDateTime.toISODate()

                const gridDay = {
                    day,
                    ISOString: dateISOString,
                    dayOfWeek: dayDateTime.weekday,
                    isInThePast: dateISOString < DateTime.now().toISODate(),
                    isActive: dateISOString === state.selectedStartDate || dateISOString === state.selectedEndDate
                }

                gridDays.push(gridDay)
                day += 1
            } else {
                gridDays.push(0)
            }
        }

        setState({ ...state, gridDays })
    }

    const handleNextMonth = () => {
        setState({
            ...state,
            selectedMonthDateTime: state.selectedMonthDateTime.plus({ month: 1 })
        })
    }

    const handlePreviousMonth = () => {
        if (minDate && state.selectedMonthDateTime.toISODate() < minDate) return

        setState({
            ...state,
            selectedMonthDateTime: state.selectedMonthDateTime.minus({ month: 1 })
        })
    }

    const handleDayOnClick = (gridDay) => {
        if (!state.selectedStartDate && !state.selectedEndDate) {
            setState({ ...state, selectedStartDate: gridDay.ISOString })
        }

        if (state.selectedStartDate && !state.selectedEndDate) {
            if (gridDay.ISOString < state.selectedStartDate) {
                setState({ ...state, selectedStartDate: gridDay.ISOString  })

            } else {
                setState({ ...state, selectedEndDate: gridDay.ISOString })
            }
        }

        if (state.selectedStartDate && state.selectedEndDate) {
            setState({
                ...state,
                selectedStartDate: gridDay.ISOString,
                selectedEndDate: null
            })
        }
    }


    return (
        <PgCalendarRootContainer kind={kind} data-cy="pg-calendar">
            <PgCalendarHeaderContainer>
                <PgCalendarArrowBackContainer onClick={handlePreviousMonth} data-cy="pg-calendar--back">
                    <PgButton icon={arrowBackIcon} kind="icon" />
                </PgCalendarArrowBackContainer>
                <PgCalendarMonthNameCalendar>
                    {state.selectedMonthDateTime.toFormat("MMMM")} {state.selectedMonthDateTime.year}
                </PgCalendarMonthNameCalendar>
                <PgCalendarNextArrowContainer onClick={handleNextMonth} data-cy="pg-calendar--next" >
                    <PgButton icon={arrowNextIcon} kind="icon" />
                </PgCalendarNextArrowContainer>
            </PgCalendarHeaderContainer>
            <PgCalendarWeekDaysNameContainer>
                {state.weekDays.map((weekDay) => (
                    <PgCalendarWeekDayContainer>
                        <PgCalendarWeekDayLabel>{weekDay}</PgCalendarWeekDayLabel>
                    </PgCalendarWeekDayContainer>
                ))}
            </PgCalendarWeekDaysNameContainer>
            <PgCalendarGridContainer>
                {state.gridDays.map((gridDay, index) => (
                    <PgCalendarGridSquareContainer
                        data-day={gridDay.ISOString}
                        key={gridDay ? `${gridDay.ISOString}__${index}` : `0__${index}`}
                        disabled={!gridDay || (disableDateInThePast ? (gridDay && gridDay.isInThePast) : false)}
                        active={(
                            gridDay.ISOString === state.selectedStartDate
                            || gridDay.ISOString === state.selectedEndDate
                        )}
                        highlight={(
                            gridDay.ISOString > state.selectedStartDate
                            && gridDay.ISOString < state.selectedEndDate
                        )}
                        onClick={() => handleDayOnClick(gridDay)}>
                        <PgCalendarDayContainer>
                            <PgButton kind="accessibility-only" text={gridDay.day} />
                        </PgCalendarDayContainer>
                    </PgCalendarGridSquareContainer>
                ))}
            </PgCalendarGridContainer>
        </PgCalendarRootContainer>
    )
}
