import React from 'react';
import { mount } from 'cypress-react-unit-test'
import { DateTime } from "luxon";
import PgCalendar from './PgCalendar';

describe("PgCalendar", () => {
    const yellow500RGB = "rgb(253, 187, 48)"


    it('should work correctly when the kind is outlined', () => {

        const startDateISO = DateTime.now().toISODate()
        const endDateISO = DateTime.now().plus({ days: 4 }).toISODate()

        mount(<PgCalendar startDate={startDateISO} endDate={endDateISO} kind="outlined" />)

        cy.getCy("pg-calendar").should(($el) => {
            expect($el).to.have.css("border", "1px solid rgb(238, 238, 238)")
        })
    });

    it('should work correctly when the kind is not set', () => {

        const startDateISO = DateTime.now().toISODate()
        const endDateISO = DateTime.now().plus({ days: 4 }).toISODate()

        mount(<PgCalendar startDate={startDateISO} endDate={endDateISO} />)

        cy.getCy("pg-calendar").should(($el) => {
            expect($el).to.have.css("border", "0px none rgb(0, 0, 0)")
        })
    });

    it("should select the range of days correctly", () => {
        const startDateISO = DateTime.now().toISODate()
        const endDateISO = DateTime.now().plus({ days: 4 }).toISODate()

        mount(<PgCalendar startDate={startDateISO} endDate={endDateISO} />)

        cy.get(`[data-day=${startDateISO}]`).should(($el) => {
            expect($el).to.have.css("background-color", yellow500RGB)
        })

        cy.get(`[data-day=${endDateISO}]`).should(($el) => {
            expect($el).to.have.css("background-color", yellow500RGB)
        })
    })
})
