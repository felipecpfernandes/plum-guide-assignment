import PgCalendar from './PgCalendar'

export default {
    title: 'Time/PgCalendar',
    component: PgCalendar,
    argTypes: {
        kind: {
            control: {
                type: 'select',
                options: ['default', 'outlined']
            }
        }
    }
}

const Template = (args) => {
    return (
        <div>
            <PgCalendar {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    disableDateInThePast: false
}
