// Libraries
import React, {useEffect, useRef, useState} from "react"

// Styles
import {
    PgDialogRootContainer,
    PgDialogOutContainer,
    PgDialogInContainer,
    PgDialogHeaderContainer,
    PgDialogBodyContainer,
    PgDialogCard
} from "./styled"

// Custom hooks
import useClickOutside from "../../customHooks/useClickOutside"

// Components
import PgButton from "../PgButton/PgButton"
import PgOverlay from "../PgOverlay/PgOverlay"

// Assets
import closeIcon from "../../assets/icons/close.svg"

// Structure components
import { PgLabel } from "../../structureComponents"


/**
 *
 * @param {boolean} active
 * @param {string} title
 * @param {string} size
 * @param {node} children
 * @param {function} onClose
 */
export default function PgDialog({
    active,
    title,
    size,
    children,
    onClose = () => ({})
}) {
    // State
    const [state, setState] = useState({
        _active: false
    })

    // Refs
    const pgDialogRef = useRef()

    // Hooks
    const { clickedOutside } = useClickOutside(pgDialogRef)

    // Watchers
    useEffect(() => {
        if (active) {
            setState({ ...state, _active: true })
            document.body.style.overflow = "hidden"
        } else {
            setState({ ...state, _active: false })
            document.body.style.overflow = "auto"
        }
    }, [active])

    useEffect(() => {
        if (clickedOutside) {
            setState({ ...state, _active: false })
            onClose()
        }
    }, [clickedOutside])

    // Methods
    const handleClose = () => {
        setState({ ...state, _active: false })
        onClose()
    }


    return (
        <PgDialogRootContainer className="pg-dialog">
            <PgOverlay active={state._active} />
            <PgDialogOutContainer active={state._active} ref={pgDialogRef} className="pg-dialog--container">
                <PgDialogInContainer>
                    <PgDialogCard className="pg-dialog--card">
                        <PgDialogHeaderContainer title={title}>
                            {title && <PgLabel xLarge semiBold>{title}</PgLabel>}
                            <PgButton
                            kind="icon"
                            className="pg-dialog--container__close"
                            icon={closeIcon}
                            iconSize="20px"
                            onClick={handleClose}/>
                        </PgDialogHeaderContainer>
                        <PgDialogBodyContainer size={size}>
                            {children}
                        </PgDialogBodyContainer>
                    </PgDialogCard>
                </PgDialogInContainer>
            </PgDialogOutContainer>
        </PgDialogRootContainer>
    )
}
