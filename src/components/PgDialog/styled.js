// Libraries
import styled from "styled-components"


export const PgDialogRootContainer = styled.div`
`

export const PgDialogOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background-color: #0000001f;
  z-index: 1000;
  opacity: ${props => props.active ? 1 : 0};
  pointer-events: ${props => props.active ? "all" : "none"};
  transition: opacity .2s ease;
`

export const PgDialogInContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  
  
  @media only screen and (max-width: 600px) {
    width: 100%;
    .pg-dialog--card {
      height: 100vh;
      width: 100%;
    } 
  }
`
export const PgDialogOutContainer = styled.div`
  position: fixed;
  left: 0;
  z-index: 2000;
  opacity: ${props => props.active ? 1 : 0};
  pointer-events: ${props => props.active ? "all" : "none"};
  transition: opacity .2s ease, margin-top .2s ease;
  margin-left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  margin-top: ${props => props.active ? "0px" : "-20px"};

  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`

export const PgDialogHeaderContainer = styled.div`
  display: flex;
  justify-content: ${props => props.title ? "space-between" : "flex-end"};
  align-items: center;
  padding-left: 20px;

  @media only screen and (max-width: 600px) {
    height: 60px;
    padding-left: 0;
  }
`
export const PgDialogBodyContainer = styled.div`
    box-sizing: border-box;
    min-width: ${props => {
        if (props.size === "small") {
            return "20vw"
        } else if (props.size === "medium") {
            return "60vw"
        } else if (props.size === "large") {
            return "80vw"
        } else {
            return "auto"
        }
    }};
`

export const PgDialogCard = styled.div`
  box-shadow: 1px 1px 8px #00000017;
  padding: 20px;
  background-color: white;
  box-sizing: border-box;
`
