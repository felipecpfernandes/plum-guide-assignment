import PgDialog from './PgDialog'
import { PgContainer, PgLabel } from "../../structureComponents";

export default {
    title: 'Containers/PgDialog',
    component: PgDialog,
    argTypes: {
        size: {
            type: "select",
            options: ["small", "medium", "large"]
        }
    }
}

const Template = (args) => {
    return (
        <div>
            <PgDialog {...args}>
                <PgContainer padding="20px">
                    <PgLabel>Welcome to the PgDialog</PgLabel>
                </PgContainer>
            </PgDialog>
        </div>
    )
}

export const master = Template.bind({ })
master.args = {
    active: true,
    title: "Book now",
    size: "small"
}
