import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgDialog from './PgDialog';

describe("PgDialog", () => {
    it('should open and close correctly', () => {
        mount(
            <PgDialog active={true}>
                <div style={{ padding: "20px" }}>
                    <label>Welcome to the PgDialog</label>
                </div>
            </PgDialog>
        )

        cy.get(`.pg-dialog--container`).should(($el) => {
            expect($el).to.have.css("opacity", '1')
        })

        cy.get(".pg-dialog--container__close").click()

        cy.get(`.pg-dialog--container`).should(($el) => {
            expect($el).to.have.css("opacity", '0')
        })
    });

    it('should have the correct title', () => {
        mount(
            <PgDialog title="Book now" size="large" active={true}>
                <div style={{ padding: "20px" }}>
                    <label>Welcome to the PgDialog</label>
                </div>
            </PgDialog>
        )

        cy.get(`.pg-dialog`).contains("Book now")
    });

})
