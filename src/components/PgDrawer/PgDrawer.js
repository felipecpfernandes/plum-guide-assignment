// Libraries
import React, {useEffect, useRef, useState} from "react"

// Styles
import {
    PgDrawerRootContainer,
    PgDrawerContainer,
    PgDrawerHeaderContainer,
    PgDrawerBodyContainer
} from "./styled"

// Components
import PgOverlay from "../PgOverlay/PgOverlay"
import PgButton from "../PgButton/PgButton"

// Custom hooks
import useClickOutside from "../../customHooks/useClickOutside"

// Icons
import closeIcon from "../../assets/icons/close.svg"


/**
 *
 * @param {boolean} active
 * @param {node} children
 * @param {string} direction
 * @param {function} onClose
 */
export default function PgDrawer({
    active,
    children,
    direction = "left",
    onClose = () => ({})
}) {
    // State
    const [state, setState] = useState({
        _active: false
    })

    // Ref
    const pgDrawerRef = useRef()

    // Hooks
    const { clickedOutside } = useClickOutside(pgDrawerRef)

    // Watchers
    useEffect(() => {
        setState({ ...state, _active: active })
    }, [active])

    useEffect(() => {
        if (clickedOutside) {
            setState({ ...state, _active: false })
            onClose()
        }
    }, [clickedOutside])

    // Methods
    const handleOnClose = () => {
        setState({ ...state, _active: false })
        onClose()
    }


    return (
        <PgDrawerRootContainer className="pg-drawer">
            <PgOverlay active={state._active} />
            <PgDrawerContainer
            className="pg-drawer--container"
            direction={direction}
            active={state._active}
            ref={pgDrawerRef}>
                <PgDrawerHeaderContainer>
                    <PgButton
                    kind="icon"
                    icon={closeIcon}
                    iconSize="18px"
                    className="pg-drawer--container__close"
                    onClick={handleOnClose} />
                </PgDrawerHeaderContainer>
                <PgDrawerBodyContainer>
                    {children}
                </PgDrawerBodyContainer>
            </PgDrawerContainer>
        </PgDrawerRootContainer>
    )
}
