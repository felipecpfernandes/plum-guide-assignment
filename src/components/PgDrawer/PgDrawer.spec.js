import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgDrawer from './PgDrawer';

describe("PgDrawer", () => {
    it('should open and close correctly with direction set as left', () => {
        mount(
            <PgDrawer active={true}>
                <div style={{ padding: "20px" }}>
                    <label>Welcome to the PgDialog</label>
                </div>
            </PgDrawer>
        )

        cy.get(`.pg-drawer--container`).should(($el) => {
            expect($el).to.have.css("margin-left", '0px')
            expect($el).to.have.css("pointer-events", 'all')
        })

        cy.get(".pg-drawer--container__close").click()

        cy.get(`.pg-drawer--container`).should(($el) => {
            expect($el).to.have.css("pointer-events", 'none')
        })
    });

    it('should open and close correctly with direction set as right', () => {
        mount(
            <PgDrawer active={true} direction="right">
                <div style={{ padding: "20px" }}>
                    <label>Welcome to the PgDialog</label>
                </div>
            </PgDrawer>
        )

        cy.get(`.pg-drawer--container`).should(($el) => {
            expect($el).to.have.css("margin-right", '0px')
            expect($el).to.have.css("pointer-events", 'all')
        })

        cy.get(".pg-drawer--container__close").click()

        cy.get(`.pg-drawer--container`).should(($el) => {
            expect($el).to.have.css("pointer-events", 'none')
        })
    });
})
