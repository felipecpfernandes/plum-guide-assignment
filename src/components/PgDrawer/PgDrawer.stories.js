import PgDrawer from './PgDrawer'

export default {
    title: 'Containers/PgDrawer',
    component: PgDrawer,
    argTypes: {
        direction: {
            type: "select",
            options: ["left", "right"]
        }
    }
}

const Template = (args) => {
    return (
        <div>
            <PgDrawer {...args} />
        </div>
    )
}

export const master = Template.bind({ })
master.args = {
    active: true,
    direction: "left"
}
