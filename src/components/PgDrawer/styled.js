// Libraries
import styled from "styled-components"


export const PgDrawerRootContainer = styled.div`
`

export const PgDrawerContainer = styled.div`
  border-top-right-radius: ${props => props.direction === "left" ? "8px" : null};
  border-bottom-right-radius: ${props => props.direction === "left" ? "8px" : null};
  border-top-left-radius: ${props => props.direction === "right" ? "8px" : null};
  border-bottom-left-radius: ${props => props.direction === "right" ? "8px" : null};
  position: fixed;
  left: ${props => props.direction === "left" ? 0 : null};
  right: ${props => props.direction === "right" ? 0 : null};
  box-shadow: 1px 1px 8px #00000017;
  top: 0;
  height: 100vh;
  width: 100%;
  max-width: 20em;
  pointer-events: ${props => props.active ? "all" : "none"};
  z-index: 2000;
  margin-left: ${props => {
     if (props.active && props.direction === "left") {
         return 0
     } 
     
     return "-20em"
  }};
  margin-right: ${props => {
    if (props.active && props.direction === "right") {
      return 0
    }

    return "-20em"
  }};
  transition: margin-left .2s ease, margin-right .2s ease;
  background-color: white;
`

export const PgDrawerHeaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 10px;
`

export const PgDrawerBodyContainer = styled.div`
    
`
