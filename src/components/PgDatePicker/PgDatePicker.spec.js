import React from 'react';
import { mount } from 'cypress-react-unit-test'
import { DateTime } from "luxon";
import PgDatePicker from './PgDatePicker';

describe("PgDatePicker", () => {
    const yellow500RGB = "rgb(253, 187, 48)"


    it('should work correctly when choosing the date range', () => {
        mount(<PgDatePicker  />)

        cy.getCy("pg-input-anchor").click()

        const startDateISO = DateTime.now().toISODate()
        const endDateISO = DateTime.now().plus({ days: 4 }).toISODate()

        cy.wait(2000)
        cy.get(`[data-day=${startDateISO}]`).click()

        cy.get(`[data-day=${startDateISO}]`).should(($el) => {
            expect($el).to.have.css("background-color", yellow500RGB)
        })

        cy.get(`[data-day=${endDateISO}]`).click()
        cy.wait(1000)

        cy.get(`[data-day=${endDateISO}]`).should(($el) => {
            expect($el).to.have.css("background-color", yellow500RGB)
        })

        const startDateLabel = DateTime.fromISO(startDateISO).toFormat("dd LLL y")
        const endDateLabel = DateTime.fromISO(endDateISO).toFormat("dd LLL y")

        const dateRangeLabel = `${startDateLabel} - ${endDateLabel}`

        cy.getCy("pg-input-anchor").contains(dateRangeLabel)
    });

    it('should work correctly when changing months', () => {
        mount(<PgDatePicker  kind="outlined" />)

        cy.getCy("pg-input-anchor").click()

        const currentMonthDate =  `${DateTime.now().toFormat("MMMM")} ${DateTime.now().year}`
        cy.getCy("pg-calendar").contains(currentMonthDate)

        cy.getCy("pg-calendar--next").click()

        const nextMonthDate =  `${DateTime.now().plus({ months: 1 }).toFormat("MMMM")} ${DateTime.now().year}`
        cy.getCy("pg-calendar").contains(nextMonthDate)

        cy.getCy("pg-calendar--back").click()
        cy.getCy("pg-calendar").contains(currentMonthDate)
    });
})
