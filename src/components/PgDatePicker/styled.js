import styled from "styled-components"

export const PgDatePickerRootContainer = styled.div`

`

export const PgDatePickerCalendarContainer = styled.div`
  box-shadow: 1px 1px 8px #00000017; 
  position: absolute;
  margin-top: ${props => props.active ? "20px" : "0"};
  opacity: ${props => props.active ? 1 : 0};
  pointer-events: ${props => props.active ? "all" : "none"};
  transition: opacity .2s ease-in-out, margin-top .2s ease-in-out;
  z-index: 100;
  visibility: ${props => !props.active ? "hidden" : "visible"};

  > * {
    pointer-events: ${props => props.active ? "all" : "none"};
  }
`
