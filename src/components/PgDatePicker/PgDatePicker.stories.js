import PgDatePicker from './PgDatePicker'

export default {
    title: 'Forms/PgDatePicker',
    component: PgDatePicker,
}

const Template = (args) => {
    return (
        <div>
            <PgDatePicker {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {

}
