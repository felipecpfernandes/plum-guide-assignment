// Libraries
import React, {useEffect, useRef, useState} from "react"
import { DateTime } from "luxon"

// Components
import PgInputAnchor from "../PgInputAnchor/PgInputAnchor";
import PgCalendar from "../PgCalendar/PgCalendar";

// Styles
import { PgDatePickerCalendarContainer, PgDatePickerRootContainer } from "./styled"

// Hooks
import useClickOutside from "../../customHooks/useClickOutside";


export default function PgDatePicker({
    autoClose,
    startDate,
    endDate,
    minDate,
    className,
    disableDateInThePast,
    onChange = () => ({})
}) {
    // Refs
    const pgDatePickerRef = useRef(null)

    //Hooks
    const { clickedOutside } = useClickOutside(pgDatePickerRef)

    // States
    const [state, setState] = useState({
        showCalendar: false,
        dateRangeLabel: "Choose date",
        _startDate: null,
        _endDate: null
    })

    // Watchers
    useEffect(() => {
        if (clickedOutside) {
            setState({ ...state, showCalendar: false })
        }
    }, [clickedOutside])

    useEffect(() => {
        if (startDate && endDate) {
            const startDateLabel = DateTime.fromISO(startDate).toFormat("dd LLL y")
            const endDateLabel = DateTime.fromISO(endDate).toFormat("dd LLL y")

            setState({
                ...state,
                _startDate: startDate,
                _endDate: endDate,
                dateRangeLabel: `${startDateLabel} - ${endDateLabel}`
            })
        }
    }, [startDate, endDate])


    // Methods
    const handleShowCalendar = (showCalendar) => {
        setState({ ...state, showCalendar })
    }

    const handleDateRangeOnChange = (dateRange) => {
        const startDateLabel = DateTime.fromISO(dateRange.startDate).toFormat("dd LLL y")
        const endDateLabel = DateTime.fromISO(dateRange.endDate).toFormat("dd LLL y")

        if (autoClose) {
            setState({
                ...state,
                dateRangeLabel: `${startDateLabel} - ${endDateLabel}`,
                showCalendar: false,
            })
        } else {
            setState({
                ...state,
                dateRangeLabel: `${startDateLabel} - ${endDateLabel}`,
            })
        }

        onChange({
            startDate: dateRange.startDate,
            endDate: dateRange.endDate
        })
    }


    return (
        <PgDatePickerRootContainer ref={pgDatePickerRef}>
            <PgInputAnchor
            label="From / To"
            width="19em"
            kind="select"
            value={state.dateRangeLabel}
            className={className}
            active={state.showCalendar}
            onChange={handleShowCalendar}/>
            <PgDatePickerCalendarContainer active={state.showCalendar}>
                <PgCalendar
                startDate={state._startDate}
                endDate={state._endDate}
                minDate={minDate}
                disableDateInThePast={disableDateInThePast}
                onChange={handleDateRangeOnChange} />
            </PgDatePickerCalendarContainer>
        </PgDatePickerRootContainer>
    )
}
