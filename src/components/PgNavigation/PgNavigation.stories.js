import PgNavigation from './PgNavigation'

export default {
    title: 'Navigation/PgNavigation',
    component: PgNavigation,
}

const Template = (args) => {
    return (
        <div>
            <PgNavigation {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    items: [
        { label: "Homes", value: "HOMES_PAGE" },
        { label: "Hosts", value: "HOSTS_PAGE" }
    ],
    uppercase: true
}
