// Libraries
import React, {useState} from "react"

// Styles
import {
    PgNavigationRootContainer,
    PgNavigationContainer,
    PgNavigationItemContainer
} from "./styled"


/**
 *
 * @param {array} items
 * @param {string} itemLabel
 * @param {string} itemValue
 * @param {boolean} uppercase
 */
export default function PgNavigation({
    items = [],
    itemLabel = "label",
    itemValue = "value",
    uppercase
}) {
    // State
    const [state, setState] = useState({
        selectedNavigationItem: null
    })

    // Methods
    const handleNavigationItemOnClick = (selectedNavigationItem) => {
        setState({ ...state, selectedNavigationItem })
    }


    return (
        <PgNavigationRootContainer>
            <PgNavigationContainer>
                {items.map((item) => (
                    <PgNavigationItemContainer
                        active={(
                            state.selectedNavigationItem
                            && item[itemValue] === state.selectedNavigationItem[itemValue]
                        )}
                        uppercase={uppercase}
                        onClick={() => handleNavigationItemOnClick(item)}>
                        <button className="pg-navigation--button">{item[itemLabel]}</button>
                    </PgNavigationItemContainer>
                ))}
            </PgNavigationContainer>
        </PgNavigationRootContainer>
    )
}
