// Libraries
import styled from "styled-components"
import {neutral700, neutral900, yellow500} from "../../assets/colorPallet/colorPallet";


export const PgNavigationRootContainer = styled.div`
  
`

export const PgNavigationContainer = styled.ul`
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;
`

export const PgNavigationItemContainer = styled.li`
    border-bottom: ${props => props.active ? `2px solid ${yellow500}` : "2px solid transparent"};
    padding-bottom: 25px;
    cursor: pointer;

    .pg-navigation--button {
      background-color: transparent;
      cursor: pointer;
      font-family: "Roboto", sans-serif;
      border: none;
      color: ${props => props.active ? neutral900 : neutral700};
      font-size: 14px;
      text-transform: ${props => props.uppercase ? "uppercase" : null};
    }
  
    &:not(:first-child) {
      margin-left: 20px;
    }
  
    &:hover {
      border-bottom: 2px solid ${yellow500};
    }
`

export const NavigationButton = styled.button`
    
`
