// Libraries
import React from 'react'

// Styles
import { PgLoadingRootContainer, PgLoadingPoint } from './styled'

// Assets
import {yellow500} from "../../assets/colorPallet/colorPallet"


export default function PgLoading(){
    const points = [1,2,3];

    return(
        <PgLoadingRootContainer>
            {points.map((_, index) => {
                return <PgLoadingPoint key={index} color={yellow500} />
            })}
        </PgLoadingRootContainer>
    )
}
