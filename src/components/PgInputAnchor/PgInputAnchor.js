// Libraries
import React, {useEffect, useState} from "react"

// Styles
import {
    PgInputAnchorContainer,
    PgInputAnchorLeftContainer,
    PgInputAnchorRightContainer,
    PgInputAnchorLabel,
    PgInputAnchorValue, PgSelectPseudo
} from "./styled"

// Components
import PgSvg from "../PgSvg/SvgIcon"

// Icons
import arrowDownIcon from "../../assets/icons/arrow-down.svg"


/**
 * @param {string} kind
 * @param {boolean} active
 * @param {string} label
 * @param {string} width
 * @param {string} placeholder
 * @param {string} value
 * @param {string} className
 * @param {function} onChange
 */
export default function PgInputAnchor({
    kind,
    active,
    label,
    width,
    placeholder = "Click here",
    value,
    className,
    onChange = () => ({})
}) {
    // State
    const [state, setState] = useState({
        internalValue: null,
        internalActive: false
    })

    // Watchers
    useEffect(() => {
        setState({ ...state, internalActive: active })
    }, [active])

    useEffect(() => {
        if (value) {
            setState({
                ...state,
                internalValue: value,
                internalActive: false
            })
        }
    }, [value])

    // Methods
    const handleOnClick = () => {
        setState({ ...state, internalActive: !state.internalActive })
        onChange(!state.internalActive)
    }

    return (
        <PgInputAnchorContainer
        clickable={kind === "select"}
        width={width}
        data-cy="pg-input-anchor"
        className={className}
        onClick={handleOnClick}>
            {kind === "select" && <PgSelectPseudo onClick={handleOnClick} width={width}  />}
            <PgInputAnchorLeftContainer>
                <PgInputAnchorLabel>{label}</PgInputAnchorLabel>
                <PgInputAnchorValue>{!state.internalValue ? placeholder : state.internalValue}</PgInputAnchorValue>
            </PgInputAnchorLeftContainer>
            {kind === "select" && (
                <PgInputAnchorRightContainer
                    active={state.internalActive}
                    data-cy="pg-input-anchor--arrow-icon-container">
                    <PgSvg src={arrowDownIcon} size="14px" />
                </PgInputAnchorRightContainer>
            )}
        </PgInputAnchorContainer>
    )
}
