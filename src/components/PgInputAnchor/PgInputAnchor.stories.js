import PgInputAnchor from './PgInputAnchor'

export default {
    title: 'Form/PgInputAnchor',
    component: PgInputAnchor,
    argTypes: {
        kind: {
            type: "select",
            options: ["default", "select"]
        }
    }
}

const Template = (args) => {
    return (
        <div>
            <PgInputAnchor {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {
    label: "From / To",
    placeholder: "Choose date",
    kind: "select",
    value: "21 Sep - 24 Sep"
}
