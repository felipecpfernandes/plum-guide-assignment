// Libraries
import styled from "styled-components"


export const PgInputAnchorContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: ${props => props.clickable ? "pointer" : null};
  width: ${props => props.width ? props.width : "10em"};
`

export const PgInputAnchorLeftContainer = styled.div`
`

export const PgInputAnchorRightContainer = styled.div`   
    padding-left: 10px;
    svg {
      transform: ${props => props.active ? "rotate(180deg)" : "rotate(360deg)"};
    }
`

export const PgInputAnchorLabel = styled.label`
    font-weight: 600;
    font-size: 14px;
    color: #7D7D80;
`

export const PgInputAnchorValue = styled.div`
    margin-top: 5px;
    font-size: 16px;
    color: #1D1D1D;   
`

export const PgSelectPseudo = styled.button`
  padding: 10px 40px 10px 10px;
  border: none !important;
  background-color: transparent;
  border-radius: 0;         
  position: absolute;
  box-shadow: none;
  width: ${props => props.width ? props.width : "10em"};
  cursor: pointer;
  z-index: 10;
  -webkit-appearance: none;
  -ms-appearance: none;
  -moz-appearance: none;
  appearance: none;
`
