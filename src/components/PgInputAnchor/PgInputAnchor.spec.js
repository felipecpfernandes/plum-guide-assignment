import React from 'react';
import { mount } from 'cypress-react-unit-test'
import PgInputAnchor from './PgInputAnchor';

describe("PgInputAnchor", () => {
    it('should render the items correctly', () => {
        mount(<PgInputAnchor label="From / To" value="21 Apr - 24 Apr" />)

        cy.getCy("pg-input-anchor").contains("From / To")
        cy.getCy("pg-input-anchor").contains("21 Apr - 24 Apr")
    });

    it('should render the items correctly when the kind select is set', () => {
        mount(<PgInputAnchor label="Guests" placeholder="Choose guests" kind="select" />)

        cy.getCy("pg-input-anchor").contains("Guests")
        cy.getCy("pg-input-anchor").contains("Choose guests")
        cy.getCy("pg-input-anchor--arrow-icon-container").should("exist")
    });
})
