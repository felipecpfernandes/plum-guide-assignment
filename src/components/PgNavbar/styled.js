// Libraries
import styled from "styled-components"
import {neutral100} from "../../assets/colorPallet/colorPallet";


export const PgNavbarRootContainer = styled.div`
    width: 100%;
    background-color: white;
    border-bottom: 1px solid ${neutral100};
    border-top: 1px solid ${neutral100};
    height: 70px;
    display: flex;
    justify-content: space-between; 
`

export const PgNavbarMenuContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  padding: 0 20px;
  border-right: 1px solid ${neutral100};
`

export const PgNavbarNavigationContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  height: 100%;
  
  @media only screen and (max-width: 750px) {
    display: none;
  }
`

export const PgNavbarBodyContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding: 0 40px;
  
  @media only screen and (max-width: 600px) {
    padding: 0 20px;
    justify-content: center;
  }
`

export const PgNavbarSearchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  padding: 0 20px;
  border-left: 1px solid ${neutral100};
  
  @media only screen and (max-width: 352px) {
    display: none;
  }
`

export const PgNavbarBodyRightContainer = styled.div`
  display: flex;
  align-items: center;

  @media only screen and (max-width: 580px) {
    display: none;
  }
`

export const PgNavigationButtonContainer = styled.div`
    &:not(:first-child) {
      margin-left: 10px;
    }
`
