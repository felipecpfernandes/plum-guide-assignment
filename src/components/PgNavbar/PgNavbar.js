// Libraries
import React, {useState} from "react"

// Styles
import {
    PgNavbarRootContainer,
    PgNavbarMenuContainer,
    PgNavbarNavigationContainer,
    PgNavbarBodyContainer,
    PgNavbarSearchContainer,
    PgNavigationButtonContainer,
    PgNavbarBodyRightContainer
} from "./styled";

// Components
import PgButton from "../PgButton/PgButton"
import PgNavigation from "../PgNavigation/PgNavigation"
import PgSvg from "../PgSvg/SvgIcon"

// Business components
import SideMenu from "../../businessComponents/SideMenu/SideMenu"
import SearchSideMenu from "../../businessComponents/SearchSideMenu/SearchSideMenu"

// Assets
import menuIcon from "../../assets/icons/menu.svg"
import searchIcon from "../../assets/icons/search.svg"
import plumGuideLogo from "../../assets/images/plum-guide-logo.svg"


export default function PgNavbar() {
    const navigationItems = [
        { label: "Homes", value: "HOMES_PAGE" },
        { label: "Hosts", value: "HOSTS_PAGE" }
    ]

    // State
    const [state, setState] = useState({
        showSideMenu: false,
        showSearchSideMenu: false
    })

    // Methods
    const handleShowSideMenu = (showSideMenu) => {
        setState({ ...state, showSideMenu })
    }

    const handleShowSearchSideMenu = (showSearchSideMenu) => {
        setState({ ...state, showSearchSideMenu })
    }

    return (
        <>
            <PgNavbarRootContainer>
                <PgNavbarMenuContainer>
                    <PgButton
                    kind="icon"
                    icon={menuIcon}
                    iconSize="18px"
                    onClick={() => handleShowSideMenu(true)} />
                </PgNavbarMenuContainer>
                <PgNavbarBodyContainer>
                    <PgNavbarNavigationContainer>
                        <PgNavigation uppercase items={navigationItems} />
                    </PgNavbarNavigationContainer>
                    <PgSvg src={plumGuideLogo} size="150px" />
                    <PgNavbarBodyRightContainer>
                        <PgNavigationButtonContainer>
                            <PgButton slimSize text="Need help?" />
                        </PgNavigationButtonContainer>
                        <PgNavigationButtonContainer>
                            <PgButton slimSize text="Login" />
                        </PgNavigationButtonContainer>
                    </PgNavbarBodyRightContainer>
                </PgNavbarBodyContainer>
                <PgNavbarSearchContainer>
                    <PgButton
                    kind="icon"
                    icon={searchIcon}
                    iconSize="18px"
                    onClick={() => handleShowSearchSideMenu(true)} />
                </PgNavbarSearchContainer>
            </PgNavbarRootContainer>
            <SideMenu
            active={state.showSideMenu}
            onClose={() => handleShowSideMenu(false)} />
            <SearchSideMenu
            active={state.showSearchSideMenu}
            onClose={() => handleShowSearchSideMenu(false)} />
        </>
    )
}
