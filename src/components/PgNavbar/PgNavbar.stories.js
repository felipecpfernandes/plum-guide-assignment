import PgNavbar from './PgNavbar'

export default {
    title: 'Navigation/PgNavbar',
    component: PgNavbar,
}

const Template = (args) => {
    return (
        <div>
            <PgNavbar {...args} />
        </div>
    )
}

export const master = Template.bind({})
master.args = {

}
