export default class Property {
    constructor({
        id,
        routeId,
        title,
        bedroomsAmount,
        bathroomsAmount,
        maxGuestsAmount,
        neighborhood,
        city,
        pricePerNight,
        images = []
    }) {
        this.id = id
        this.routeId = routeId
        this.title = title
        this.bedroomsAmount = bedroomsAmount
        this.bathroomsAmount = bathroomsAmount
        this.maxGuestsAmount = maxGuestsAmount
        this.neighborhood = neighborhood
        this.city = city
        this.pricePerNight = pricePerNight
        this.images = images
    }

    /**
     * This getter returns a string with the short addess of this property.
     * Example: Notting Hill, London
     */
    get shortAddress() {
        return `${this.neighborhood}, ${this.city}`
    }

    /**
     * This getter returns a string with the amounts of guests allowed in this property
     * Example: 4 people
     */
    get maxGuestsLabel() {
        if (this.maxGuestsAmount === 1) {
            return "1 person"
        }

        if (this.maxGuestsAmount > 1) {
            return `${this.maxGuestsAmount} people`
        }

        return ""
    }

    /**
     * This getter returns a string with the amounts of bedrooms in this property
     * Example: 2 bedrooms
     */
    get bedroomsLabel() {
        if (this.bedroomsAmount === 1) {
            return "1 bedroom"
        }

        if (this.bedroomsAmount > 1) {
            return `${this.bedroomsAmount} bedrooms`
        }

        return ""
    }

    /**
     * This getter returns a string with the amounts of bathrooms in this property
     * Example: 2 bathrooms
     */
    get bathroomsLabel() {
        if (this.bathroomsAmount === 1) {
            return "1 bathroom"
        }

        if (this.bathroomsAmount > 1) {
            return `${this.bathroomsAmount} bathrooms`
        }

        return ""
    }

    /**
     * This getter returns the guests options based on the max guests amount for this property
     */
    get guestsOptions() {
        const _guestsOptions = []

        for (let i = 0; i <= this.maxGuestsAmount - 1; i += 1) {
            let guestLabel = "guests"
            if (i === 0) {
                guestLabel = "guest"
            }

            _guestsOptions.push({
                label: `${i + 1} ${guestLabel}`,
                value: i + 1
            })
        }

        return _guestsOptions
    }
}
