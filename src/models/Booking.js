import { DateTime } from "luxon"

export default class Booking {
    constructor({
        id,
        startDate,
        endDate,
        guestsAmount,
        pricePerNight,
        propertyId
    }) {
        this.id = id
        this.startDate = startDate
        this.endDate = endDate
        this.guestsAmount = guestsAmount
        this.pricePerNight = pricePerNight
        this.propertyId = propertyId
    }

    /**
     * This getter returns the number of nights booked
     */
    get totalNights() {
        const nights = Math.round(DateTime.fromISO(this.endDate).diff(DateTime.fromISO(this.startDate), "days").days)
        return nights
    }

    /**
     * This getter returns a string with the amount of nights booked
     * Example: 2 nights
     */
    get totalNightsLabel() {
        if (this.totalNights === 1) {
            return "1 night"
        }

        return `${this.totalNights} nights`
    }

    /**
     * This getter returns the number of the total price for this booking
     */
    get totalPrice() {
        return Math.floor(this.pricePerNight * this.totalNights)
    }

    /**
     * This getter returns a string with the range date for this booking
     * Example: 21 Sep 2021 - 24 Sep 2021
     */
    get dateRangeLabel() {
        const startDateLabel = DateTime.fromISO(this.startDate).toFormat("dd LLL y")
        const endDateLabel = DateTime.fromISO(this.endDate).toFormat("dd LLL y")

        return `${startDateLabel} - ${endDateLabel}`
    }
}
