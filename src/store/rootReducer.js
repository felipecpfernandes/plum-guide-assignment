import { combineReducers } from 'redux'
import properties from './modules/properties'
import bookings from "./modules/bookings"


export default combineReducers({
    properties,
    bookings
})
