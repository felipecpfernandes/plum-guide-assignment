import {SET_CURRENT_BOOKING} from "./mutations";

export const setCurrentBooking = ({ booking }) => async (dispatch) => {
    dispatch({ type: SET_CURRENT_BOOKING, payload: booking })
}
