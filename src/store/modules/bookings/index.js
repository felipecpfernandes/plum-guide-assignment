import { SET_CURRENT_BOOKING } from "./mutations"
import Booking from "../../../models/Booking"

const initialState = {
    currentBooking: new Booking({})
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case SET_CURRENT_BOOKING:
            return { ...state, currentBooking: action.payload }

        default:
            return state;
    }
}

export default reducer;
