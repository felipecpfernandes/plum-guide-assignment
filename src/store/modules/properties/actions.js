// Handlers
import PropertyHandler from "../../../handlers/PropertyHandler"

// Mutations
import {SET_CURRENT_PROPERTY} from "./mutations"

export const fetchPropertyByRouteId = ({ routeId }) => async (dispatch) => {
    const property = await PropertyHandler.fetchPropertyByRouteId({ routeId })
    dispatch({ type: SET_CURRENT_PROPERTY, payload: property })
}
