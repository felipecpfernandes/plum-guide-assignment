import { SET_CURRENT_PROPERTY } from "./mutations"

const initialState = {
    currentProperty: false
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case SET_CURRENT_PROPERTY:
            return { ...state, currentProperty: action.payload }

        default:
            return state;
    }
}

export default reducer;
