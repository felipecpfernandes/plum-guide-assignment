// Libraries
import axios from "axios"

// Mock data
import {getPropertyByRouteId} from "../data/MockData"

// Models
import Property from "../models/Property"


export default class PropertyHandler {
    /**
     * This method can be used to get a property by the routeId
     *
     * @param {string} routeId
     */
    static async fetchPropertyByRouteId({ routeId }) {
        const response = await getPropertyByRouteId({ routeId })

        const imagesResponse = await axios.get(
            "https://run.mocky.io/v3/8dac4388-ce28-4406-95bb-91aec813168d"
        )

        let images = []
        if (imagesResponse && imagesResponse.data) {
            const { imageUrls } = imagesResponse.data
            images =  imageUrls.splice(0, 30)
        }

        const property = new Property({
            ...response.data,
            images
        })

        return property
    }
}
