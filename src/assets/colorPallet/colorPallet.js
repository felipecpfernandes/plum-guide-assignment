export const yellow500 = "#fdbb30"
export const yellow100 = "#fff7eb"

export const neutral900 = "#1d1d1d"
export const neutral700 = "#636366"
export const neutral100 = "#eee"
