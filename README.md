# Plum guide (code challenge)
Welcome to the Plum Guide code challenge! Please follow the next steps to run the project locally


### Running locally-
- Open your terminal and CD into the folder where this project is located. 
- On the root folder, type `npm install` to install all the dependencies 
- On the root folder, type `npm start` to start the application 
- The application should be running on `http://localhost:3000`

### Cypress tests
To run the cypress tests, go to the root of the project and type `npm run cypress`

### Storybook
- Make sure your project is running on port 3000
- To run the storybook, go to the root of the project and type `npm run storybook`
- The storybook should be running on `http://localhost:6006`

### Live demo
https://plum-guide-assignment.vercel.app
