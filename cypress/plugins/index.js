const injectDevServer = require("@cypress/react/plugins/react-scripts")
const webpackPreprocessor = require('@cypress/webpack-preprocessor')
const defaults = webpackPreprocessor.defaultOptions

module.exports = (on, config) => {
    require('@cypress/code-coverage/task')(on, config)
    delete defaults.webpackOptions.module.rules[0].use[0].options.presets
    on('file:preprocessor', webpackPreprocessor(defaults))
    injectDevServer(on, config)
    return config
}
